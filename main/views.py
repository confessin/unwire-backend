# Create your views here.
import datetime

import django_filters
from main.models import Workspace, Amenities, Bookings, UnwireUser, Offers
from rest_framework import viewsets
from main.serializers import WorkspaceSerializer, AmenitiesSerializer, BookingsSerializer
from main.serializers import UserSerializer, OfferSerializer
from rest_framework import filters
from rest_framework import generics


class WorkspaceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Workspace.objects.all().prefetch_related('amenities').order_by('-rating')
    serializer_class = WorkspaceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('location','city')


class AmenitiesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Amenities.objects.all()
    serializer_class = AmenitiesSerializer


class BookingsFilter(filters.FilterSet):
    user1 = django_filters.CharFilter(name="user__phone_number")

    class Meta:
        model = Bookings
        fields = ['user1', ]


class BookingsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Bookings.objects.all()
    serializer_class = BookingsSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('phone_number',)
    filter_class = BookingsFilter


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = UnwireUser.objects.all()
    serializer_class = UserSerializer

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('phone_number',)


class OfferFilter(filters.FilterSet):
    start_date1 = django_filters.DateTimeFilter(name="start_date", lookup_type='lte')
    end_date1 = django_filters.DateTimeFilter(name="end_date", lookup_type='gte')

    class Meta:
        model = Offers
        fields = ['start_date1', 'end_date1', 'is_featured', 'is_active']


class OffersViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Offers.objects.all()
    serializer_class = OfferSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = OfferFilter
