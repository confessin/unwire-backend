#!/usr/bin/env python
# encoding: utf-8

"""
Description
"""

__author__ = 'mohammad.rafi@inmobi.com (Mohammad Rafi)'

from rest_framework import serializers
from main.models import Workspace, Amenities, WorkspaceAmenitiesMap, Bookings, UnwireUser
from main.models import Offers
from django.conf import settings


class WAMapSerializer(serializers.ModelSerializer):

    class Meta:
        model = Amenities

        fields = ('name', 'value', 'typex', 'icon')


class WorkspaceSerializer(serializers.ModelSerializer):
    amenities1 = WAMapSerializer(source='amenities', many=True)

    class Meta:
        model = Workspace
        fields = (
                'id',
                'name',
                'location',
                'latitude',
                'longitude',
                'city',
                'rating',
                'phone_number',
                'minimum_order',
                'description',
                'base_image',
                'description',
                'opening_time',
                'closing_time',
                'amenities1'
                )


class AmenitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amenities
        fields = ('name', 'value', 'typex', 'icon')


class BookingsSerializer(serializers.ModelSerializer):

    workspace_name = serializers.ReadOnlyField(source='workspace.name', read_only=True)
    workspace_id = serializers.ReadOnlyField(source='workspace.id')
    #workspace_image = serializers.ReadOnlyField(source='workspace.base_image', read_only=True)
    workspace_image = serializers.SerializerMethodField('get_thumbnail_url', read_only=True)

    def get_thumbnail_url(self, obj):
        return '%s%s' % (settings.MEDIA_URL, obj.workspace.base_image)

    class Meta:
        model = Bookings
        fields = ('id', 'user', 'workspace', 'workspace_name', 'workspace_image', 'workspace_id', 'coupon_applied', 'coupon_id', 'checkin_time', 'need_dongle', 'num_tables')


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = UnwireUser
        fields = ('id', 'name', 'phone_number', 'is_active')


class OfferSerializer(serializers.ModelSerializer):
    workspace_name = serializers.ReadOnlyField(source='workspace.name', read_only=True)
    workspace_id = serializers.ReadOnlyField(source='workspace.id', read_only=True)
    workspace_image = serializers.SerializerMethodField('get_thumbnail_url', read_only=True)

    def get_thumbnail_url(self, obj):
        return '%s%s' % (settings.MEDIA_URL, obj.workspace.base_image)

    class Meta:
        model = Offers
        fields = ('detail', 'start_date', 'end_date', 'is_featured',
                  'workspace_name', 'workspace_id', 'workspace_image')
