from django.contrib import admin
from main.models import Amenities
from main.models import Workspace
from main.models import CancellationPolicy
from main.models import Images
from main.models import WorkspaceAmenitiesMap
from main.models import Bookings
from main.models import Coupon
from main.models import UnwireUser
from main.models import Offers

# Register your models here.


class UnwireUserAdmin(admin.ModelAdmin):
    pass


class AmenitiesAdmin(admin.ModelAdmin):
    pass


class WorkspaceAdmin(admin.ModelAdmin):
    pass


class CancellationpolicyAdmin(admin.ModelAdmin):
    pass


class ImagesAdmin(admin.ModelAdmin):
    pass


class WAMap(admin.ModelAdmin):
    pass


class BookingsAdmin(admin.ModelAdmin):
    pass


class CouponsAdmin(admin.ModelAdmin):
    pass


class OffersAdmin(admin.ModelAdmin):
    pass

admin.site.register(UnwireUser, UnwireUserAdmin)
admin.site.register(Amenities, AmenitiesAdmin)
admin.site.register(Workspace, WorkspaceAdmin)
admin.site.register(CancellationPolicy, CancellationpolicyAdmin)
admin.site.register(Images, ImagesAdmin)
admin.site.register(WorkspaceAmenitiesMap, WAMap)
admin.site.register(Bookings, BookingsAdmin)
admin.site.register(Coupon, CouponsAdmin)
admin.site.register(Offers, OffersAdmin)
